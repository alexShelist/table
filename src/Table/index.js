import React, { Component } from "react";

// Import React Table
import ReactTable from "react-table";
import "react-table/react-table.css";

import Chance from "chance";
import checkboxHOC from "react-table/lib/hoc/selectTable";
import cloneDeep from 'clone-deep';

const CheckboxTable = checkboxHOC(ReactTable);
const chance = new Chance();

export default class Table extends Component {
  constructor(props) {
    super(props);
    const data = this.getData();
    let pointer = localStorage.getItem('pointer');
    let actions = localStorage.getItem('actions');
    actions = (actions) ? JSON.parse(actions) : [data];
    this.state = {
      data,
      selection: [],
      selectAll: false,
      rebuild: false,
      actions,
      pointer,
    };
  }

  componentDidMount() {
    if (typeof window !== 'undefined') {
      window.addEventListener('storage', this.localStorageUpdated)
    }
  }
  componentWillUnmount(){
    if (typeof window !== 'undefined') {
      window.removeEventListener('storage', this.localStorageUpdated)
    }
  }

  getData = () => {
    let data = localStorage.getItem('data');
    data = (data) ? JSON.parse(data) : [];
    return data.map(item => {
      const _id = chance.guid();
      return {
        _id,
        ...item
      };
    });
  };

  localStorageUpdated = () => {
    let data = localStorage.getItem('data');
    data = (data) ? JSON.parse(data) : [];
    let pointer = localStorage.getItem('pointer');
    let actions = localStorage.getItem('actions');
    actions = (actions) ? JSON.parse(actions) : [data];
    this.setState({ data, pointer, actions });
  };

  renderEditable = (cellInfo) => {
    return (
      <div
        style={{ backgroundColor: "#fafafa" }}
        contentEditable
        suppressContentEditableWarning
        onBlur={e => {
          const data = cloneDeep(this.state.data);
          if (data[cellInfo.index][cellInfo.column.id] !== e.target.innerHTML) {
            data[cellInfo.index][cellInfo.column.id] = e.target.innerHTML;
            this.addAction(data);
            this.setState({data});
          }
        }}
        dangerouslySetInnerHTML={{
          __html: this.state.data[cellInfo.index][cellInfo.column.id]
        }}
      />
    );
  };

  renderActions = (cellInfo) => {
    return (
      <button className='btn btn-danger btn-sm' onClick={() => this.deleteRow(cellInfo.original._id)}>Delete Row</button>
    );
  };

  addAction = data => {
    localStorage.setItem('data', JSON.stringify(data));
    let actions = cloneDeep(this.state.actions);
    let pointer = this.state.pointer;
    pointer++;
    actions.splice(pointer, actions.length);
    actions.push(data);
    localStorage.setItem('pointer', pointer);
    localStorage.setItem('actions', JSON.stringify(actions));
    this.setState({actions, pointer});
  };

  addNewRow = () => {
    let data = cloneDeep(this.state.data);
    let selection = cloneDeep(this.state.selection);
    const id = chance.guid();
    data.push({
      _id: id,
      item: '',
      cost: '',
    });
    selection.push(id);
    this.addAction(data);
    this.setState({data, selection});
  };

  deleteRow = (id) => {
    let data = cloneDeep(this.state.data);
    data.forEach((item, index) => {
      if (item._id === id) data.splice(index, 1)
    });
    this.addAction(data);
    this.setState({data});
  };

  toggleSelection = (key, shift, row) => {
    let selection = [...this.state.selection];
    const keyIndex = selection.indexOf(key);
    if (keyIndex >= 0) {
      selection = [
        ...selection.slice(0, keyIndex),
        ...selection.slice(keyIndex + 1)
      ];
    } else {
      selection.push(key);
    }
    this.setState({selection});
  };

  toggleAll = () => {
    const selectAll = !this.state.selectAll;
    const selection = [];
    if (selectAll) {
      const wrappedInstance = this.checkboxTable.getWrappedInstance();
      const currentRecords = wrappedInstance.getResolvedState().sortedData;
      currentRecords.forEach(item => {
        selection.push(item._original._id);
      });
    }
    this.setState({selectAll, selection});
  };

  isSelected = key => {
    return this.state.selection.includes(key);
  };

  rebuildTable = () => {
    this.setState({rebuild: true});
  };

  saveTable = () => {
    this.setState({rebuild: false});
    let idsDel = [];
    let data = cloneDeep(this.state.data);
    data.forEach((item, index) => {
      if (!this.state.selection.includes(item._id)) {
        idsDel.push(index)
      }
    });
    for (let i = idsDel.length - 1; i >= 0; i--) {
      data.splice(idsDel[i], 1)
    }
    this.addAction(data);
    this.setState({data});
  };

  undoHistory = () => {
    let actions = cloneDeep(this.state.actions);
    let pointer = this.state.pointer;
    pointer--;
    if (actions[pointer]) {
      localStorage.setItem('pointer', pointer);
      localStorage.setItem('data', JSON.stringify(actions[pointer]));
      localStorage.setItem('actions', JSON.stringify(actions));
      this.setState({data: actions[pointer], pointer});
    }
  };

  redoHistory = () => {
    let actions = cloneDeep(this.state.actions);
    let pointer = this.state.pointer;
    pointer++;
    if (actions[pointer]) {
      localStorage.setItem('pointer', pointer);
      localStorage.setItem('data', JSON.stringify(actions[pointer]));
      localStorage.setItem('actions', JSON.stringify(actions));
      this.setState({data: actions[pointer], pointer});
    }
  };

  render() {
    const {toggleSelection, toggleAll, isSelected} = this;
    const {data, selectAll} = this.state;
    const columns = [
      {
        Header: "Item",
        accessor: "item",
        Cell: this.renderEditable
      },
      {
        Header: "Cost per pound or kg",
        accessor: "cost",
        Cell: this.renderEditable
      },
      {
        Header: "Actions",
        Cell: this.renderActions
      },
    ];

    const checkboxProps = {
      selectAll,
      isSelected,
      toggleSelection,
      toggleAll,
      selectType: "checkbox",
      getTrProps: (s, r) => {
        const selected = this.isSelected(r && r.original && r.original._id);
        return {
          style: {
            backgroundColor: selected ? "lightgreen" : "inherit"
          }
        };
      }
    };

    return (
      <div className="container">
        <div className="row my-3">
          <div className="col-4">
            <button onClick={this.undoHistory} disabled={!this.state.actions[Number(this.state.pointer) - 1]} className="btn btn-primary btn-sm">Undo</button>
            <button onClick={this.redoHistory} disabled={!this.state.actions[Number(this.state.pointer) + 1]} className="btn btn-primary btn-sm ml-3">Redo</button>
          </div>
          <div className="col-4">
            <button onClick={this.addNewRow} className='btn btn-primary'>Add New Row</button>
          </div>
          <div className="col-4">
            {(!this.state.rebuild) ?
            <button onClick={this.rebuildTable} className='btn btn-primary'>Rebuild Table</button> :
            <button onClick={this.saveTable} className='btn btn-primary'>Save</button>}
          </div>
        </div>

        {!this.state.rebuild ?
          <div>
            <ReactTable
              data={data}
              columns={columns}
              defaultPageSize={10}
              className="-striped -highlight"
            />
            <br/>

          </div>
          :
          <CheckboxTable
            ref={r => (this.checkboxTable = r)}
            data={data}
            columns={columns}
            defaultPageSize={10}
            className="-striped -highlight"
            {...checkboxProps}
          />}
      </div>
    );
  }
}
